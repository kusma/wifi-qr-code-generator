var QRCode = require('qrcode')

function wifiString(ssid, encryption, password, hidden) {
  function escape(str) {
    return str.replace(/[";,:\\]/g, function (x) {
      return '\\' + x;
    });
  }
  return 'WIFI:S:' + escape(ssid) + ';T:' + escape(encryption) + ';P:' + escape(password) + ';H:' + escape(hidden.toString()) + '>;';
}

function update() {
  var ssid = document.getElementById('ssid').value;
  var encryption = document.querySelector('input[name=encryption]:checked').value;
  var no_encryption = encryption === '';
  document.getElementById('password').disabled = no_encryption;
  document.getElementById('password-line').classList.toggle('hidden', no_encryption);
  var password = no_encryption ? '' : document.getElementById('password').value;

  var opts = {
    type: 'svg',
    margin: 0,
  };
  QRCode.toString(wifiString(ssid, encryption, password, false), opts, function (err, str) {
    if (err)
      throw err;

    var img = document.getElementById('qr');
    img.src = 'data:image/svg+xml; charset=utf8, ' + encodeURIComponent(str);
    var img = document.getElementById('render-ssid').textContent = ssid;
    var img = document.getElementById('render-password').textContent = password;

    var rect = document.getElementById("text-layer").getBBox();
    document.getElementById("text").setAttribute("viewBox", "-" + (rect.width / 2) + " 0 " + rect.width + " " + (no_encryption ? 17 : 33));
  })
}

document.getElementById('form').onchange = update;
document.getElementById('ssid').oninput = update;
document.getElementById('password').oninput = update;
update();
